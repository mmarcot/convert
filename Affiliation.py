# coding: utf-8

class Affiliation:
    """
    Classe qui représente une affilition (cad le laboratoire) dans lequel travaille
    un ou plusieurs auteurs
    """

    def __init__(self, id_aff, name_aff):
        """
        Constructeur d'une affiliation
        """
        self.id_aff = str(id_aff)
        self.name_aff = name_aff.strip()
