# coding: utf-8

from lxml import etree
import lxml.html
import config
import os, os.path
import urllib.request

from Article import Article
from Author import Author
from Affiliation import Affiliation


class ApjParser:
    """
    Classe qui permet de parser des fichiers venant de l'ApJ
    """
    def __init__(self, filename):
        """
        Constructeur d'un parser ApJ
            - filename : le nom du fichier d'entrée xml contenant les données a parser
        """
        self.filename = filename
        self.articles = []  # liste des articles trouvés


    def parse(self):
        """
        MEthode qui va s'occuper d'extraire les données du fichier XML
        Retourne une liste d'Article
        """
        config.logger.info("Parsing the ApJ input file ...")

        # on crée un liste d'element tree (1 etree = 1 article) :
        fich = open(self.filename)
        parser = etree.XMLParser(resolve_entities=False)
        root_tree = etree.parse(fich, parser)
        trees = []  # liste des articles sous forme de etree
        for node in root_tree.xpath("//stk_header"):
            trees.append(etree.ElementTree(node))

        # on parcours la liste des articles et on les construits 1 à 1 :
        for tree in trees:
            # init attributs Article :
            year = self.getYear(tree)
            journal_name = self.getJournal_name(tree)
            tome_number = self.getTome_number(tree)
            article_number = self.getArticle_number(tree)
            page_start = self.getPage_start(tree)
            page_nb = self.getPage_nb(tree)
            authors = self.getAuthors(tree)
            affiliations = self.getAffiliations(tree)
            title = self.getTitle(tree)
            abstract = self.getAbstract(tree)
            keywords = self.getKeywords(tree)
            doi = self.getDoi(tree)
            objects = self.getObjects(tree)
            copyright = self.getCopyright(tree)
            special_char = 'L' if config.input_filename[:4] == "ApJL" else '.'

            # on ajoute l'article courant à la liste des articles :
            self.articles.append(Article(year,
                                 journal_name,
                                 tome_number,
                                 article_number,
                                 page_start,
                                 page_nb,
                                 authors,
                                 affiliations,
                                 title,
                                 abstract,
                                 keywords,
                                 doi,
                                 objects,
                                 copyright,
                                 special_char))

            # config.logger.info("Article " + title + " [OK]")

        fich.close()
        config.logger.info("ApJ File SUCCESSFULLY parsed")

        return self.articles


    def getYear(self, art_tree):
        """
        Methode qui s'occupe d'extraire l'attribut year du fichier
            - art_tree: l'ElementTree contenant l'article
        """
        year = art_tree.xpath("//dates/date_cover/text()")

        if len(year) > 0:
            year = int(year[0][:4])
        else:
            year = -1
            config.logger.info("WARNING year not found")

        return year

    def getJournal_name(self, art_tree):
        """
        Methode qui s'occupe d'extraire l'attribut journal_name du fichier
            - art_tree: l'ElementTree contenant l'article
        """
        return config.journal_name

    def getTome_number(self, art_tree):
        """
        Methode qui s'occupe d'extraire l'attribut tome_number du fichier
            - art_tree: l'ElementTree contenant l'article
        """
        tome_number = art_tree.xpath("//ident/volume/text()")

        if len(tome_number) > 0:
            tome_number = int(tome_number[0])
        else:
            tome_number = -1
            config.logger.info("WARNING tome_number not found")

        return tome_number

    def getArticle_number(self, art_tree):
        """
        Methode qui s'occupe d'extraire l'attribut article_number du fichier
            - art_tree: l'ElementTree contenant l'article
        """
        article_number = art_tree.xpath("//ident/artnum/text()")

        if len(article_number) > 0:
            article_number = int(article_number[0].replace("L", ""))  # enlève le L dans les letters
        else:
            article_number = -1
            config.logger.info("WARNING article_number not found")

        return article_number

    def getPage_start(self, art_tree):
        """
        Methode qui s'occupe d'extraire l'attribut page_start du fichier
            - art_tree: l'ElementTree contenant l'article
        """
        page_start = art_tree.xpath("//ident/pages/@start")

        if len(page_start) > 0:
            page_start = int(page_start[0].replace("L", ""))
        else:
            page_start = -1
            config.logger.info("WARNING page_start not found")

        return page_start


    def getPage_nb(self, art_tree):
        """
        Methode qui s'occupe d'extraire l'attribut page_nb du fichier
            - art_tree: l'ElementTree contenant l'article
        """
        page_nb = art_tree.xpath("//ident/pages/@extent")

        if len(page_nb) > 0:
            page_nb = int(page_nb[0])
        else:
            page_nb = -1
            config.logger.info("WARNING page_nb not found")

        return page_nb

    def getAuthors(self, art_tree):
        """
        Methode qui s'occupe d'extraire l'attribut authors du fichier
            - art_tree: l'ElementTree contenant l'article
        """
        aut_trees = []  # liste des auteurs sous forme de etree
        for node in art_tree.xpath("//authors/author_granular"):
            aut_trees.append(etree.ElementTree(node))

        authors = []
        for aut_tree in aut_trees:  # boucle de parcours des auteurs pour 1 article
            aut_given = aut_tree.xpath("//given/text()")
            aut_surname = aut_tree.xpath("//surname/text()")
            aut_affil = aut_tree.xpath("//@affil")

            if len(aut_given) > 0 and len(aut_surname) > 0 and len(aut_affil) > 0:
                authors.append(Author(prenom=aut_given[0], nom=aut_surname[0], affil=aut_affil[0].split(",")))
            elif len(aut_given) > 0 and len(aut_surname) > 0:
                authors.append(Author(prenom=aut_given[0], nom=aut_surname[0]))
            elif len(aut_surname) > 0:
                authors.append(Author(nom=aut_surname[0]))
            else:
                config.logger.error("ERROR while parsing author {}".format(etree.tostring(aut_tree)))

        return authors

    def getAffiliations(self, art_tree):
        """
        Methode qui s'occupe d'extraire l'attribut affiliations du fichier
            - art_tree: l'ElementTree contenant l'article
        """
        aff_trees = []  # liste des affiliations sous forme de etree
        for node in art_tree.xpath("//authors/affil"):
            aff_trees.append(etree.ElementTree(node))

        affiliations = []
        for aff_tree in aff_trees:
            affs = aff_tree.xpath("//text()")
            affs_id = aff_tree.xpath("//@id")

            if len(affs) > 0 and len(affs_id) > 0:
                affiliations.append(Affiliation(affs_id[0], affs[0]))
            else:
                config.logger.error("ERROR while parsing affiliation {}".format(etree.tostring(aff_tree)))

        return affiliations

    def getTitle(self, art_tree):
        """
        Methode qui s'occupe d'extraire l'attribut title du fichier
            - art_tree: l'ElementTree contenant l'article
        """
        tit = art_tree.xpath("//title/title_toc/descendant-or-self::*/text()")

        title = []
        if len(tit) > 0:
            title.append("".join(tit).strip())

        if len(title) > 0:
            title = title[0]
        else:
            title = ""
            config.logger.info("WARNING title not found")

        return title

    def getAbstract(self, art_tree):
        """
        Methode qui s'occupe d'extraire l'attribut abstract du fichier et qui crée
        un dossier local contenant les images
            - art_tree: l'ElementTree contenant l'article
        """
        # on crée le dossier si il n'existe pas déjà :
        try :
            os.mkdir(self.filename+"-img")
        except FileExistsError:
            pass

        # abs_tree = art_tree.xpath("//header_text[@heading='ABSTRACT']/descendant-or-self::*/text() | //header_text[@heading='ABSTRACT']/descendant-or-self::*/@SRC")
        abs_tree = art_tree.xpath("//header_text[@heading='ABSTRACT']")

        if len(abs_tree) > 0 : # si il y a 1 abstract :
            str_feed = etree.tostring(abs_tree[0]) # 1 abstract
            doc = lxml.html.fromstring(str_feed)

            os.chdir(self.filename+"-img")

            # après chaque balise img, on ajoute un element perso contenant le lien de l'image
            for elem_img in doc.iter(tag="img"):
                # on recupère le nom de l'image :
                url_distant = elem_img.get("src")
                img_name = url_distant.split("/")[-1]

                # on enregistre l'image si elle n'existe pas déjà :
                if not os.path.isfile(img_name) :
                    urllib.request.urlretrieve(url_distant, img_name)

                # on crée un nouvel élément etree, on lui ajoute du text, puis on l'ajoute au document HTML :
                el = etree.Element("a")
                el.text = " {" + os.getcwd() + "/" + img_name + "} "
                elem_img.addnext(el)

            os.chdir("..")

            abstract = str(doc.text_content())
        else:
            abstract = ""

        return ' '.join([' '.join(line.split()) for line in abstract.splitlines() if line.strip()]) # suppr mult spaces


    def getKeywords(self, art_tree):
        """
        Methode qui s'occupe d'extraire l'attribut keywords du fichier
            - art_tree: l'ElementTree contenant l'article
        """
        keywords = art_tree.xpath("//kwd_main/text()")

        if len(keywords) == 0:
            config.logger.info("WARNING keywords not found")

        return keywords


    def getDoi(self, art_tree):
        """
        Methode qui s'occupe d'extraire l'attribut doi du fichier
            - art_tree: l'ElementTree contenant l'article
        """
        doi = art_tree.xpath("//ident/doi/text()")

        if len(doi) > 0:
            doi = doi[0]
        else:
            doi = ""
            config.logger.info("WARNING doi not found")

        return doi


    def getObjects(self, art_tree):
        """
        Methode qui s'occupe d'extraire l'attribut objects du fichier
            - art_tree: l'ElementTree contenant l'article
        """
        return [] # on n'extrait pas les objets pour l'ApJ


    def getCopyright(self, art_tree):
        """
        Methode qui extrait le copyright d'un article
            - art_tree: l'ElementTree contenant l'article
        """
        cr = art_tree.xpath("//copyright_text/text()")

        if len(cr) > 0:
            cr = cr[0]
        else:
            cr = ""
            config.logger.info("WARNING copyright not found")

        return cr
