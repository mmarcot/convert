# coding: utf-8

import config
import os.path
import os
from ObjExtractor import ObjExtractor



class Article:
    """
    Classe représentant 1 article
    """

    # static var :
    simbad_set = set()  # set des auteurs présents dans Simbad

    def __init__(self, year=-1, journal_name="", tome_number=-1, article_number=-1, page_start=-1, page_nb=-1, authors=[], affiliations=[], title="", abstract="", keywords=[], doi="", objects=[], copyright="", special_char=".", bibcode=""):
        """
        Constructeur d'un article
        Si le bibcode n'est pas donné en parametre, il sera calculer à partir des données
        disponible
        """
        self.year = year  # l'année de parution du magazine
        self.journal_name = journal_name  # le nom du journal
        self.tome_number = tome_number  # le numéro de volume du journal
        self.article_number = article_number  # le numéro de l'article concerné
        self.page_start = page_start
        self.page_nb = page_nb  # le nombre de page de l'article
        self.authors = authors  # la liste des auteurs de la publication
        self.affiliations = affiliations  # la liste des affiliations
        self.title = title.encode("ascii", "ignore").decode("ascii")  # le titre de l'article
        self.abstract = abstract  # le résumé de l'article
        self.keywords = keywords  # la liste des mots clés
        self.doi = doi  # l'id de l'article
        self.objects = objects  # la liste des objets associés
        self.copyright = copyright # la str contenant le copyright
        self.special_char = special_char.upper()  # caractère joker du bibcode

        if not bibcode:  # si on a pas le bibcode en argument, on le construit :
            self.bibcode = self.__constructBibcode()
        else:  # si on a le bibcode et pas les données correspondante, on extrait les données :
            self.bibcode = bibcode.strip()
            if year == -1 :
                self.year = self.bibcode[:4]
            if tome_number == -1:
                self.tome_number = self.bibcode[9:13].strip(".")
            if article_number == -1:
                self.article_number = self.bibcode[14:18].strip(".")
            if page_start == -1:
                self.page_start = self.bibcode[14:18].strip(".")


    def __constructBibcode(self):
        """
        Methode qui s'occupe de construire le bibcode
        YYYYJJJJJVVVVMPPPPA
        """
        if len(str(self.year)) == 4:
            year_bib = str(self.year)
        else:
            config.logger.error("ERROR : year attrib must have 4 chars : YYYY")
            raise ValueError("ERROR : year attrib must have 4 chars : YYYY ! current value: " + str(self.year))

        journal_bib = self.journal_name[:5] + (5 - len(self.journal_name[:5])) * "."
        volume_bib = (4 - len(str(self.tome_number))) * "." + str(self.tome_number)
        page_bib = (4 - len(str(self.page_start))) * "." + str(self.page_start)
        author_bib = self.authors[0].nom[0]

        return year_bib + journal_bib + volume_bib[-4:] + self.special_char + page_bib + author_bib.upper()


    def getAuthors(self):
        """
        Methode qui renvoie tous les auteurs sous forme de chaine de caractère
        prête a être inséré dans le parfile
        """
        ch = ""
        for aut in self.authors:
            ch += "%A " + aut.getParfileFormat(True) + "\n"

        return ch.strip()  # .encode("ascii", "ignore").decode("ascii")


    def getAffiliations(self):
        """
        Methode qui renvoie tous les affiliations sous forme de chaine de caractère
        prête a être inséré dans le parfile
        """
        ch = ""
        for aff in self.affiliations:
            ch += "%I " + aff.name_aff
            if aff.id_aff:
                ch += " (" + aff.id_aff + ")"
            ch += "\n"

        return ch.strip()


    def getKeywords(self):
        """
        Methode qui renvoie tous les keywords sous forme de chaine de caractère
        prête a être inséré dans le parfile
        """
        ch = ""
        for kw in self.keywords:
            ch += "%K " + kw + "\n"

        return ch.strip().encode("ascii", "ignore").decode("ascii")


    def getObjects(self):
        """
        Methode qui renvoie tous les objets sous forme de chaine de caractère
        prête a être inséré dans le parfile
        """
        ch = ""
        for obj in self.objects:
            ch += "%O " + obj + "\n"

        return ch.strip()


    def testAuthors(self):
        """
        Methode qui effectue un ensemble de tests et eventuellement de correction
        sur les auteurs
        """
        # chargement du set des auteurs Simbad :
        if len(Article.simbad_set) == 0 :
            fich = open(os.path.join(config.install_path, "simref.0"))
            for line in fich.readlines():
                line = line.strip()
                if line == "" :
                    continue
                if line[:2].upper() == "%A":
                    line = line[2:].strip()
                    Article.simbad_set.add(line)
            fich.close()


        for aut in self.authors:
            if not aut.getSimbadFormat() in Article.simbad_set:
                config.logger.warning(aut.getSimbadFormat())


    def testTitle(self, correct_data):
        """
        MEthode qui effectue un ensemble de tests et eventuellement de correction
        sur le titre de l'article
        Le methode check chaque mot 1 par 1, si le mot est dans le fichier alors on
        utilise le format contenu dans le fichier, sinon on le met en minuscule
        """
        # on construit le dictionnaire des mots à tester puis changer :
        fich = open(os.path.join(config.install_path, "cvn.maj.words"))
        dico = dict()
        for line in fich.readlines():
            line = line.strip()
            # si la ligne est vide ou si c'est un commentaire, on skip :
            if line == "" or line[0] == '#' :
                continue

            word1, word2 = line.split()[0], line.split()[1]
            dico[word1] = word2
        fich.close()

        title_cpy_low = self.title.lower()
        ntitle = ""
        for mot in title_cpy_low.split():
            if mot in dico:
                ntitle += dico[mot]
            else:
                ntitle += mot

            ntitle += " "

        if correct_data:
            self.title = ntitle.strip()
            self.title = self.title[0].upper() + self.title[1:]  # 1ere lettre en MAJ
        elif ntitle != self.title:
            config.logger.warning("Title of " + self.bibcode + " is not well formed !")


    def testObjects(self):
        """
        Methode qui test les différents objets trouvés dans l'article et dit si ils
        sont trouvés dans Simbad ou non
        """
        # on regarde si il y a un fichier .obj lié au fichier XML :
        filepath_obj = config.file_path.rsplit(".", 1)[0] + ".obj"
        try:
            fich = open(filepath_obj)
            fich.close()
            objextr = ObjExtractor(filepath_obj)
            objextr.extract()
        except FileNotFoundError:
            config.logger.error(filepath_obj + " file not found !!!")
