# coding: utf-8

import config

from Article import Article
from Author import Author
from Affiliation import Affiliation


class AstbuParser:
    """
    Classe qui permet de parser des fichiers venant de l'AstBu
    """
    def __init__(self, filename):
        """
        Constructeur d'un parser AstBu
            - filename : le nom du fichier d'entrée xml contenant les données a parser
        """
        self.filename = filename
        self.articles = []  # liste des articles trouvés


    def parse(self):
        """
        MEthode qui va s'occuper d'extraire les données du fichier XML
        Retourne une liste d'Article
        """
        config.logger.info("Parsing the AstBu input file ...")

        # on crée un liste d'element tree (1 etree = 1 article) :
        fich = open(self.filename)

        ls_art = [] # contient toutes les lignes concernant 1 article
        KEY, VALUE = 0, 1
        current_key = ""  # la clée en cours (cas des attributs sur plusieurs lignes)
        for line in fich.readlines():
            # on remplit la liste ls_art :
            if line[0] == "%" or line[:4] == "DOI:" or line[:4] == "    ":  # nouvel attribut :
                ls_art.append( (line[:4],line[4:].strip()) )
                if line[:4] != "    ":
                    current_key = line[:4]
            else:
                continue  # rien d'interessant, on skip


            if ls_art[-1][KEY].strip() == '%R':  # nouvel article :
                ls_art.pop()
                # instanciation de l'article :
                if ls_art:
                    self.articles.append(self.constructArticle(ls_art))
                # clean la liste :
                ls_art = []
                ls_art.append( (line[:4],line[4:]) )
            elif ls_art[-1][KEY] == '    ':  # nouvelle ligne, même attribut :
                poped = ls_art.pop()
                # print("### "+str((current_key, poped[VALUE] + line[4:])))
                ls_art.append( (current_key, poped[VALUE]) )

        # on construit le dernier article :
        self.articles.append(self.constructArticle(ls_art))
        fich.close()
        config.logger.info("AstBu File SUCCESSFULLY parsed")

        return self.articles


    def constructArticle(self, ls_art):
        """
        Methode qui instancie un Article grace à la liste passée en parametre
        Retourne l'article correspondant à cette liste
        """

        # on construit la liste d'auteurs :
        authors = []
        i = 0
        for aut in self.getValueOf("%A", ls_art) :
            i += 1
            nom = aut.split(",")[0]
            prenom = aut.split(",")[1]
            authors.append(Author(nom=nom, prenom=prenom, affil=[i]))

        # on construit la liste d'affiliations :
        affiliations = []
        i = 0
        for aff in self.getValueOf("%I", ls_art):
            i += 1
            # print("==>" + aff)
            affiliations.append(Affiliation(i, aff.strip()))

        bibcode = self.getValueOf("%R", ls_art)[0]
        title = " ".join(self.getValueOf("%T", ls_art))
        abstract = " ".join(self.getValueOf("%B", ls_art))

        keywords = " ".join(self.getValueOf("%K", ls_art)).split(",")
        keywords_striped = []
        for kw in keywords:
            keywords_striped.append(kw.strip())

        doi = self.getValueOf("DOI:", ls_art)[0]
        objects = self.getValueOf("%O", ls_art)

        return Article(authors=authors,
                        affiliations=affiliations,
                        title=title,
                        abstract=abstract,
                        keywords=keywords_striped,
                        doi=doi,
                        objects=objects,
                        bibcode=bibcode)


    def getValueOf(self, key, ls_art):
        """
        Methode qui retourne la valeur correspondante à la clée passée en
        parametre dans un fichier parfile
        la methode retourne les resultats sous forme de liste
        """
        KEY, VALUE = 0, 1
        res = []
        for tu in ls_art:
            if tu[KEY].strip() == key:
                res.append(tu[VALUE].strip())

        return res
