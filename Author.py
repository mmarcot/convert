# coding: utf-8

import config
import os.path
import unicodedata



class Author:
    """
    Classe qui représente un auteur
    """

    def __init__(self, prenom="", nom="", fullname="", affil=[]):
        """
        Constructeur d'un auteur
            - nom : le nom de l'auteur (surname)
            - prenom : le ou les prenom(s) de l'auteur (given)
            - affil : la liste des id de laboratoires dont dépend l'auteur
        """
        self.prenom = prenom.strip()
        self.nom = nom.strip()
        self.fullname = fullname.strip()
        self.affil = affil

        if prenom == "" and nom == "":
            self.__parseNames()


    def getParfileFormat(self, with_affil=True):
        """
        Methode qui retourne le prénom et le nom de l'auteur correctement formaté pour le parfile
            - with_affil: ajouter les affiliations entre parenthese ?
        """
        ret = self.nom + ", " + self.prenom

        if with_affil:
            for aff in self.affil:
                if aff:
                    ret += " (" + str(aff) + ")"

        return ret


    def getSimbadFormat(self):
        """
        Methode qui retourne le prénom et le nom de l'auteur correctement formaté pour le parfile
            - with_affil: ajouter les affiliations entre parenthese ?
        """
        ret = ""
        if self.nom[:2].upper() == "MC":
            ret += "Mc" + self.nom[2:].upper() + " "
        else:
            ret += self.nom.upper() + " "

        for word in self.prenom.split():
            ret += word[0].upper() + "."

        return self.utf8ToASCII(ret)


    def utf8ToASCII(self, s):
        """
        Essaie de convertir de l'utf-8 à l'ASCII, but UTF-8 is a superset of ASCII.
        Either your UTF-8 file is ASCII, or it can't be converted without loss.
        """
        ch = ''.join(c for c in unicodedata.normalize('NFD', s)
                      if unicodedata.category(c) != 'Mn')
        ch = ch.replace("'", "")

        a_ch = ch.encode("ascii", "ignore")

        return a_ch.decode("utf-8")


    def __parseNames(self):
        """
        Methode qui parse l'attribut fullname pour tenter de ressortir le prénom
        et le nom de l'auteur
        """
        # le dernier mot est le nom et le reste (devant) sont les prénoms :
        liste_mots = self.fullname.split()
        nb_mot = len(liste_mots)
        i = 0
        for mot in liste_mots:
            if i < nb_mot - 1:
                self.prenom += mot + " "
            else:
                self.nom += mot
            i += 1
