# coding: utf-8

import config
import os

from Article import Article
from Author import Author


class GcnParser:
    """
    Classe qui permet de parser des fichiers venant de GCN
    """
    def __init__(self, filename):
        """
        Constructeur d'un parser GCN
            - filename : le nom du fichier d'entrée xml contenant les données a parser
        """
        self.filename = filename
        self.articles = []  # liste des articles trouvés


    def parse(self):
        """
        MEthode qui va s'occuper d'extraire les données du fichier XML
        Retourne une liste d'Article
        """
        config.logger.info("Parsing the GCN input file ...")

        # on crée un liste d'element tree (1 etree = 1 article) :
        fich = open(self.filename)

        titre_journal = ""
        number = ""
        subject = ""
        date = ""
        author = ""
        abstract = ""

        # current_key = ""  # la clée en cours (cas des attributs sur plusieurs lignes)
        is_abstract = False
        for line in fich.readlines():
            if line.find("TITLE:") == 0:
                titre_journal = line.replace("TITLE:", "").strip()
            if line.find("NUMBER:") == 0:
                number = line.replace("NUMBER:", "").strip()
            elif line.find("SUBJECT:") == 0:
                subject = line.replace("SUBJECT:", "").strip()
            elif line.find("DATE:") == 0:
                date = line.replace("DATE:", "").strip()
                date = date[:2] # extrait year
                # format year :
                if int(date) < 50:
                    date = "20" + date
                else:
                    date = "19" + date
            elif line.find("FROM:") == 0:
                author = line.replace("FROM:", "").strip()
                author = author.split(" at ")[0].strip()
                author_nom = author.split(" ")[-1]
                author_prenom = author.replace(author_nom, "").strip()

            if is_abstract :
                abstract += line.strip()

            if titre_journal and number and subject and date and author :
                is_abstract = True

            # on extrait le nom d'objet du titre :
            mots = subject.split()
            objet = []
            if len(mots) > 0 and mots[0].upper() == "GRB":
                ch = mots[0] + " " + mots[1]
                objet.append(ch.strip(" :"))


        self.articles.append(Article(
            journal_name=config.journal_name,
            article_number=number,
            tome_number=number,
            page_start=1,
            title=subject,
            year=date,
            authors=[Author(nom=author_nom, prenom=author_prenom)],
            abstract=abstract,
            objects=objet
            ))

        fich.close()
        config.logger.info("GCN File SUCCESSFULLY parsed")

        return self.articles


