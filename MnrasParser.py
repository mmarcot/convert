# coding: utf-8

from lxml import etree
import config
import pprint

from Article import Article
from Author import Author
from Affiliation import Affiliation


class MnrasParser:
    """
    Classe qui permet de parser des fichiers venant de l'MNRAS
    """
    def __init__(self, filename):
        """
        Constructeur d'un parser MNRAS
            - filename : le nom du fichier d'entrée xml contenant les données a parser
        """
        self.filename = filename
        self.articles = []  # liste des articles trouvés


    def parse(self):
        """
        MEthode qui va s'occuper d'extraire les données du fichier XML
        Retourne une liste d'Article
        """
        config.logger.info("Parsing the MNRAS input file ...")

        # on crée un liste d'element tree (1 etree = 1 article) :
        fich = open(self.filename)
        parser = etree.XMLParser(resolve_entities=False)
        root_tree = etree.parse(fich, parser)
        trees = []  # liste des articles sous forme de etree
        for node in root_tree.xpath("//front"):
            trees.append(etree.ElementTree(node))

        for tree in trees:
            # init attributs Article :
            year = self.getYear(tree)
            journal_name = self.getJournal_name(tree)
            tome_number = self.getTome_number(tree)
            article_number = self.getArticle_number(tree)
            page_start = self.getPage_start(tree)
            page_nb = self.getPage_nb(tree)
            authors = self.getAuthors(tree)
            affiliations = self.getAffiliations(tree)
            title = self.getTitle(tree)
            abstract = self.getAbstract(tree)
            keywords = self.getKeywords(tree)
            doi = self.getDoi(tree)
            objects = self.getObjects(tree)
            copyright = self.getCopyright(tree)

            # on ajoute l'article courant à la liste des articles :
            self.articles.append(Article(year,
                                 journal_name,
                                 tome_number,
                                 article_number,
                                 page_start,
                                 page_nb,
                                 authors,
                                 affiliations,
                                 title,
                                 abstract,
                                 keywords,
                                 doi,
                                 objects,
                                 copyright))

            config.logger.info("Article " + title + " [OK]")

        fich.close()
        config.logger.info("MNRAS File SUCCESSFULLY parsed")

        return self.articles


    def getYear(self, art_tree):
        """
        Methode qui s'occupe d'extraire l'attribut year du fichier
            - art_tree: l'ElementTree contenant l'article
        """
        year = art_tree.xpath("//pub-date[@pub-type='epub']/year/text()")

        if len(year) > 0:
            year = int(year[0][:4])
        else:
            year = -1
            config.logger.info("WARNING year not found")

        return year

    def getJournal_name(self, art_tree):
        """
        Methode qui s'occupe d'extraire l'attribut journal_name du fichier
            - art_tree: l'ElementTree contenant l'article
        """
        return config.journal_name

    def getTome_number(self, art_tree):
        """
        Methode qui s'occupe d'extraire l'attribut tome_number du fichier
            - art_tree: l'ElementTree contenant l'article
        """
        tome_number = art_tree.xpath("//volume/text()")

        if len(tome_number) > 0:
            tome_number = int(tome_number[0])
        else:
            tome_number = -1
            config.logger.info("WARNING tome_number not found")

        return tome_number

    def getArticle_number(self, art_tree):
        """
        Methode qui s'occupe d'extraire l'attribut article_number du fichier
            - art_tree: l'ElementTree contenant l'article
        """
        article_number = art_tree.xpath("//fpage/text()")

        if len(article_number) > 0:
            article_number = int(article_number[0])
        else:
            article_number = -1
            config.logger.info("WARNING article_number not found")

        return article_number

    def getPage_start(self, art_tree):
        """
        Methode qui s'occupe d'extraire l'attribut page_start du fichier
            - art_tree: l'ElementTree contenant l'article
        """
        page_start = art_tree.xpath("//fpage/text()")

        if len(page_start) > 0:
            page_start = int(page_start[0])
        else:
            page_start = -1
            config.logger.info("WARNING page_start not found")

        return page_start


    def getPage_nb(self, art_tree):
        """
        Methode qui s'occupe d'extraire l'attribut page_nb du fichier
            - art_tree: l'ElementTree contenant l'article
        """
        page_nb = art_tree.xpath("//page-count/@count")

        if len(page_nb) > 0:
            page_nb = int(page_nb[0])
        else:
            page_nb = -1
            config.logger.info("WARNING page_nb not found")

        return page_nb

    def getAuthors(self, art_tree):
        """
        Methode qui s'occupe d'extraire l'attribut authors du fichier
            - art_tree: l'ElementTree contenant l'article
        """
        authors = []
        for node in art_tree.xpath("//contrib[@contrib-type='author']") :
            aut_tree = etree.ElementTree(node)
            # nom de famille :
            surname_aut = aut_tree.xpath("//name/surname/text()")
            # on mets les prénoms les uns après les autres dans une chaine de
            # caractere car il peut y avoir plusieurs prénoms :
            given_aut = aut_tree.xpath("//name/given-names/text()")
            givens = ""
            for giv in given_aut:
                givens += " " + giv
            givens = givens.strip()
            # affiliations :
            affs = aut_tree.xpath("//xref[@ref-type='aff']/sup/text()")

            authors.append(Author(nom=surname_aut[0], prenom=givens, affil=affs))

        if len(authors) == 0:
            config.logger.info("WARNING authors not found")

        return authors

    def getAffiliations(self, art_tree):
        """
        Methode qui s'occupe d'extraire l'attribut affiliations du fichier
            - art_tree: l'ElementTree contenant l'article
        """
        aff_trees = []  # liste des affiliations sous forme de etree
        for node in art_tree.xpath("//contrib-group/aff"):
            aff_trees.append(etree.ElementTree(node))

        affiliations = []
        for aff_tree in aff_trees:
            affs = aff_tree.xpath("//text()")
            affs_id = aff_tree.xpath("//label/text()")

            if len(affs) > 0 and len(affs_id) > 0:
                affiliations.append(Affiliation(affs_id[0], " ".join(affs)))
            elif len(affs) > 0:
                affiliations.append(Affiliation("", " ".join(affs)))
            else:
                config.logger.error("ERROR while parsing affiliation {}".format(etree.tostring(aff_tree)))

        return affiliations

    def getTitle(self, art_tree):
        """
        Methode qui s'occupe d'extraire l'attribut title du fichier
            - art_tree: l'ElementTree contenant l'article
        """
        tit = art_tree.xpath("//article-title/descendant-or-self::*/text()")

        title = []
        if len(tit) > 0:
            title.append("".join(tit).strip())

        if len(title) > 0:
            title = title[0]
        else:
            title = ""
            config.logger.info("WARNING title not found")

        return title

    def getAbstract(self, art_tree):
        """
        Methode qui s'occupe d'extraire l'attribut abstract du fichier
            - art_tree: l'ElementTree contenant l'article
        """
        abst = art_tree.xpath("//abstract/descendant-or-self::*/text()")

        abstract = []
        if len(abst) > 0:
            abstract.append("".join(abst).strip())

        if len(abstract) > 0:
            abstract = abstract[0]
        else:
            abstract = ""
            config.logger.info("WARNING abstract not found")

        return abstract

    def getKeywords(self, art_tree):
        """
        Methode qui s'occupe d'extraire l'attribut keywords du fichier
            - art_tree: l'ElementTree contenant l'article
        """
        keywords = art_tree.xpath("//kwd-group/kwd/text()")

        if len(keywords) == 0:
            config.logger.info("WARNING keywords not found")

        return keywords

    def getDoi(self, art_tree):
        """
        Methode qui s'occupe d'extraire l'attribut doi du fichier
            - art_tree: l'ElementTree contenant l'article
        """
        doi = art_tree.xpath("//article-id[@pub-id-type='doi']/text()")

        if len(doi) > 0:
            doi = doi[0]
        else:
            doi = ""
            config.logger.info("WARNING doi not found")

        return doi

    def getObjects(self, art_tree):
        """
        Methode qui s'occupe d'extraire l'attribut objects du fichier
            - art_tree: l'ElementTree contenant l'article
        """
        return []  # on extrait pas les objets pour MNRAS


    def getCopyright(self, art_tree):
        """
        Methode qui extrait le copyright d'un article
            - art_tree: l'ElementTree contenant l'article
        """
        cr = art_tree.xpath("//copyright-statement/text()")

        if len(cr) > 0:
            cr = cr[0]
        else:
            cr = ""
            config.logger.info("WARNING copyright not found")

        return cr
