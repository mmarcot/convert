import config
import os


class ObjExtractor:
    """
    Classe qui permet de :
        - sortir l'ensemble des objets relatés dans un fichier XML A+A (grace au fichier .obj)
        - verifier si il est présent dans Simbad
        - écrire la sortie
    """

    def __init__(self, obj_filename):
        """
        Constructeur d'un ObjExtractor
            - filename : nom du fichier .obj
        """
        self.obj_filename = obj_filename
        self.objects_set = set()
        self.obj_in_simbad = []
        self.obj_not_in_simbad = []


    def extract(self):
        """
        Methode qui extrait les noms d'objets, effectue le check Simbad, et produit le fichier de sortie (obj.lis)
        """
        config.logger.debug("Extracting obj of " + self.obj_filename)

        fich = open(self.obj_filename)

        for line in fich.readlines():
            res = ""
            res = line.split("}{")[1]  # on extrait la partie qui nous interesse
            res = res.replace("\nobreakspace", "")
            res = res.replace("{}", "")

            # on ajoute l'objet courant dans le set :
            self.objects_set.add(res)

        config.logger.debug("" + str(len(self.objects_set)) + " object(s) found : " + str(self.objects_set))

        self.__simbadCheck()
        self.__writeOutput()


    def __simbadCheck(self):
        """
        Methode qui va vérifier si l'objet est dans Simbad ou non
        """
        for obj in self.objects_set:
            res_cmd = os.popen("rsimbad -Ubibmgr -Pcheck_obj astrotypes=@ " + obj).read()
            if res_cmd.find("!***") != -1:  # objet non trouvé dans Simbad :
                config.logger.warning("Object '" + obj + "' not found in Simbad")
                self.obj_not_in_simbad.append(obj)
            else:  # obj in Simbad :
                self.obj_in_simbad.append(obj)


    def __writeOutput(self):
        """
        Methode qui va écrire le fichier obj.lis listant l'ensemble des objets
        avec le fichier d'origine, permettant de garder le lien
        """
        fich = open( config.filedir_path.rstrip("/") + "/obj.lis", "w" )
        for obj in self.objects_set:
            fich.write("" + self.obj_filename.split("/")[-1] + " " + obj + "\n")
        fich.close()
