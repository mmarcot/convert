# coding: utf-8

import config
import os.path
import Author

class OutputWriter:
    """
    Classe qui s'occupe de la gestion de la sortie (parfile, fichier de MAJ
    Simbad...)
    """

    def __init__(self, articles):
        """
        Constructeur d'un OutputWriter
            - articles: les données à écrire
        """
        self.articles = articles


    def writeParfile(self):
        """
        Methode qui formatte les données sous forme de fichier parfile
        /!\ ATTENTION : En cas de modification dans cette méthode il faut modifier
                        en conséquence le ParefileParser ...
        """
        fich = open(os.path.join(config.filedir_path, config.output_filename), "w")

        for article in self.articles:
            if article.bibcode:
                fich.write("%R " + article.bibcode + "\n")
            fich.write("%F " + config.input_filename + "\n")
            if article.title:
                fich.write("%T " + article.title + "\n")
            if article.authors:
                fich.write(article.getAuthors() + "\n")
            if article.affiliations:
                fich.write(article.getAffiliations() + "\n")
            if article.doi:
                fich.write("%DOI " + article.doi + "\n")
            if article.copyright :
                fich.write("%c " + article.copyright + "\n")
            if article.keywords:
                fich.write(article.getKeywords() + "\n")
            if article.objects:
                fich.write(article.getObjects() + "\n")
            if article.abstract:
                fich.write("%B " + article.abstract + "\n")

            fich.write("\n\n")


        fich.close()


    def writeSimbadUpdateFile(self):
        """
        Methode qui formatte les données sous forme de fichier de MAJ Simbad
        """
        fich = open(os.path.join(config.filedir_path, config.output_filename), "w")

        for article in self.articles:
            if article.bibcode:
                fich.write("b " + article.bibcode + "\n")

            if article.title:
                fich.write("A T " + article.title + "\n")

            if article.copyright:
                fich.write("A C " + article.copyright + "\n")

            if article.doi:
                fich.write("A D " + article.doi + "\n")

            if article.page_nb:
                fich.write("A L " + str(article.page_nb) + "\n")

            if article.authors:
                ch = "A A "
                for aut in article.authors:
                    ch += aut.getSimbadFormat() + ", "
                fich.write(ch.strip(", ") + "\n")

            if article.keywords:
                ch = "A K "
                for kw in article.keywords:
                    ch += kw + " - "
                fich.write(ch.strip("- ") + "\n")

            # commentaire de travail :
            fich.write("A CT =0=\n")

            # commentaire attaché à la ref ?
            fich.write("A CR =A=\n")

            if article.abstract:
                fich.write("A B " + article.abstract + "\n")

            fich.write("V\n\n")


        # ecriture des objets à la fin du fichier de maj :

        # on définit la lettre dans l'expression permettant de mettre à jour SImbad :
        lettre = "o"  # car objet directement tagé par l'editeur
        if config.journal_name.lower() == "gcn" :
            lettre = "t"  # car objet tiré du titre

        # ecriture des objets :
        for article in self.articles:
            if article.objects :
                ch = ""
                for obj in article.objects :
                    ch += "O " + obj + "\n"
                    ch += "A R "  + article.bibcode + ", " + lettre + ", " + obj + "\n"
                    ch += "V\n\n"
                fich.write(ch)


        fich.close()
