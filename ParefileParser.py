# coding: utf-8

import config
import re

from Article import Article
from Author import Author
from Affiliation import Affiliation


class ParefileParser:
    """
    Classe qui permet de parser un parefile préalablement générer par ce convert
    """
    def __init__(self, filename):
        """
        Constructeur d'un parser Parefile
            - filename : le nom du fichier d'entrée contenant les données a parser
        """
        self.filename = filename
        self.articles = []  # liste des articles trouvés


    def parse(self):
        """
        MEthode qui va s'occuper d'extraire les données du fichier XML
        Retourne une liste d'Article
        """
        config.logger.info("Parsing the Parefile input file ...")

        # on crée un liste d'element tree (1 etree = 1 article) :
        fich = open(self.filename)

        ls_art = [] # contient toutes les lignes concernant 1 article
        KEY, VALUE = 0, 1
        # current_key = ""  # la clée en cours (cas des attributs sur plusieurs lignes)
        for line in fich.readlines():

            if not line.strip():  # ligne vide :
                if len(ls_art) > 0:
                    self.articles.append(self.constructArticle(ls_art))
                ls_art = []
            else:
                # on remplit la liste ls_art :
                if line[0] == "%":  # nouvel attribut :
                    ls_art.append( (line[:2],line[2:].strip()) )
                else:  # ligne suivante :
                    prec = ls_art.pop()
                    ls_art.append( (prec[KEY], prec[VALUE] + " " + line.strip()) )

        # on construit le dernier article :
        if len(ls_art) > 0:
            self.articles.append(self.constructArticle(ls_art))

        fich.close()
        config.logger.info("Parefile File SUCCESSFULLY parsed")

        return self.articles


    def constructArticle(self, ls_art):
        """
        Methode qui instancie un Article grace à la liste passée en parametre
        Retourne l'article correspondant à cette liste
        """
        # on construit la liste d'auteurs :
        authors = []
        i = 0
        for aut in self.getValueOf("%A", ls_art) :
            aut = re.sub(r'\([^)]*\)', '', aut)  # on enleve les parenthese
            nom = aut.split(",")[0]
            prenom = aut.split(",")[1]
            authors.append(Author(nom=nom, prenom=prenom, affil=[i]))

        # on construit la liste d'affiliations :
        affiliations = []
        i = 0
        for aff in self.getValueOf("%I", ls_art):
            i += 1
            # print("==>" + aff)
            affiliations.append(Affiliation(i, aff.strip()))

        bibcode = self.getValueOf("%R", ls_art)[0]
        title = self.getValueOf("%T", ls_art)[0]
        copyright = ""
        try:
            copyright = self.getValueOf("%c", ls_art)[0]
        except IndexError:
            pass

        doi = ""
        try:
            doi = self.getValueOf("%DOI", ls_art)[0]
        except IndexError:
            pass

        abstract = []
        try:
            abstract = self.getValueOf("%B", ls_art)[0]
        except IndexError:
            pass

        keywords = []
        try:
            keywords = self.getValueOf("%K", ls_art)
        except IndexError:
            pass

        objects = self.getValueOf("%O", ls_art)


        return Article(authors=authors,
                        title=title,
                        abstract=abstract,
                        keywords=keywords,
                        objects=objects,
                        copyright=copyright,
                        bibcode=bibcode,
                        doi=doi,
                        affiliations=affiliations)


    def getValueOf(self, key, ls_art):
        """
        Methode qui retourne la valeur correspondante à la clée parefile passée en
        parametre
        la methode retourne les resultats sous forme de liste
        """
        KEY, VALUE = 0, 1
        res = []
        for tu in ls_art:
            if tu[KEY].strip() == key and tu[VALUE].strip() :
                res.append(tu[VALUE].strip())

        return res
