# README #

*convert* est un programme développé en Python qui permet de convertir des articles reçu dans des formats **variés** (XML, ASCII, JSON ...) en un format de sortie **unique**

Il s'agit donc de parser des fichiers divers, d'en extraire les informations qui nous intéresse, et de formaliser la sortie du programme