# coding: utf-8

import logging
import os.path
from logging.handlers import RotatingFileHandler


 ### LISTE DES VARIABLES ###
 # logger : contient le logger
 # file_path : le path vers le fichiers éditeur
 # filedir_path : le path vers le dossier contenant le fichier éditeur
 # install_path : le path où se situe le script python main.py
 # cwd_path : le path courant de l'user
 # journal_name : le nom du journal tiré à partir du nom de dossier
 # input_filename : le nom du fichier éditeur


# création de l'objet logger qui va nous servir à écrire dans les logs
logger = logging.getLogger()
# on met le niveau du logger à DEBUG, comme ça il écrit tout
logger.setLevel(logging.DEBUG)

# création d'un formateur qui va ajouter le temps, le niveau
# de chaque message quand on écrira un message dans le log
formatter = logging.Formatter('%(asctime)s :: %(levelname)s :: %(message)s')
# création d'un handler qui va rediriger une écriture du log vers
# un fichier en mode 'append', avec 1 backup et une taille max de 1Mo
file_handler = RotatingFileHandler(os.path.realpath(__file__).replace(os.path.basename(__file__), "activity.log"), 'a', 1000000, 1)
# on lui met le niveau sur DEBUG, on lui dit qu'il doit utiliser le formateur
# créé précédement et on ajoute ce handler au logger
file_handler.setLevel(logging.DEBUG)
file_handler.setFormatter(formatter)
logger.addHandler(file_handler)

# création d'un second handler qui va rediriger chaque écriture de log
# sur la console
stream_handler = logging.StreamHandler()
stream_handler.setLevel(logging.DEBUG)
logger.addHandler(stream_handler)

# les path :
file_path = ""
filedir_path = ""
install_path = ""
cwd_path = ""

# autre :
journal_name = ""
input_filename = ""
output_filename = ""
