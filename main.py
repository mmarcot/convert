# coding: utf-8

import config
import os, os.path
from ApjParser import ApjParser
from AaParser import AaParser
from MnrasParser import MnrasParser
from AstbuParser import AstbuParser
from OutputWriter import OutputWriter
from IbvsParser import IbvsParser
from GcnParser import GcnParser
from ParefileParser import ParefileParser
import argparse



def getParser():
    """
    Fonction qui renvoie le bon parser en fonction du dossier courant
    """
    if args["test"] or args["simbad"] :
        return ParefileParser(config.file_path)
    elif config.journal_name.lower() == "a+a":
        return AaParser(config.file_path)
    elif config.journal_name.lower() == "aj" or config.journal_name.lower() == "apj" or config.journal_name.lower() == "apjs":
        return ApjParser(config.file_path)
    elif config.journal_name.lower() == "astbu":
        return AstbuParser(config.file_path)
    elif config.journal_name.lower() == "gcn":
        return GcnParser(config.file_path)
    elif config.journal_name.lower() == "ibvs":
        return IbvsParser(config.file_path)
    elif config.journal_name.lower() == "mnras":
        return MnrasParser(config.file_path)



################################################################################
#                                ARGUMENTS PARSER                              #
################################################################################

# Initialisation du parser d'arguments:
arg_parser = argparse.ArgumentParser(description="Programme de conversion")
arg_parser.add_argument('file', type=str, help='File to convert')  # nargs ?
arg_parser.add_argument('-t', '--test', action='store_true', help='To test a parfile')
arg_parser.add_argument('-s', '--simbad', action='store_true', help='To generate Simbad update file from parfile')
arg_parser.add_argument('-u', '--update', action='store_true', help='Update Simbad with the input file')
arg_parser.add_argument('-o', '--output', action='store', help='The name of the output file')
args = vars(arg_parser.parse_args())


# Gestion de l'algo en fonction des arguments :
extract_data_bool = True
test_data_bool = True
write_parfile_bool = True
write_simbad_update_bool = False
update_simbad_bool = False

if args["simbad"]:
    write_parfile_bool = False
    write_simbad_update_bool = True
    test_data_bool = False

if args["test"]:
    # extract_data_bool = False
    test_data_bool = True
    write_parfile_bool = False

if args["update"]:
    extract_data_bool = False
    test_data_bool = False
    write_parfile_bool = False
    write_simbad_update_bool = False
    update_simbad_bool = True

if args["output"]:
    config.output_filename = args["output"]
else:
    config.output_filename = "output.txt"


config.logger.debug("""bool state
    extract_data_bool : {}
    test_data_bool : {}
    write_parfile_bool : {}
    write_simbad_update_bool : {}
    update_simbad_bool : {}
    """.format(extract_data_bool, test_data_bool, write_parfile_bool, write_simbad_update_bool, update_simbad_bool))


################################################################################
#                                   MAIN                                       #
################################################################################

# on construit le path vers le fichier à parser:
config.cwd_path = os.getcwd()
config.file_path = os.path.join(os.getcwd(), args["file"])
config.filedir_path = os.path.join(os.getcwd(), args["file"]).replace("/" + os.path.basename(args["file"]), "")
config.install_path = os.path.realpath(__file__).replace("/" + os.path.basename(__file__), "")
config.journal_name = config.filedir_path.split("/")[-1]
config.input_filename = os.path.basename(args["file"])

# Extraction des données:
articles = []
if extract_data_bool:
    parser = getParser()
    articles = parser.parse()
    articles.sort(key=lambda x: x.page_start)  # trié par bibcode croissant

    # on extrait les numero d'article :
    ls_page_start = []
    for art in articles :
        ls_page_start.append(art.page_start)
    ls_page_start.sort()

    # vérif la présence de tous les articles :
    cpt = min(ls_page_start)
    for nb in ls_page_start:
        if cpt == nb : # pas de manquant
            cpt += 1
        elif cpt == nb - 1 :  # 1 manquant
            config.logger.info("Article #" + str(cpt) + " is missing")
            cpt += 2
        else :  # plusieurs manquants
            for manq in range(cpt, nb):
                config.logger.info("Article #" + str(cpt) + " is missing")
                cpt += 1
            cpt += 1



# Test et éventuellement correction des données:
if test_data_bool:
    for art in articles:
        if config.journal_name.lower() == "aj" or config.journal_name.lower() == "apj" or config.journal_name.lower() == "apjs":
            art.testTitle(True)
        art.testAuthors()
        if config.journal_name.lower() == "a+a":
            art.testObjects()

# Gestion de la sortie :
output_writer = OutputWriter(articles)
if write_parfile_bool:
    output_writer.writeParfile()
if write_simbad_update_bool:
    output_writer.writeSimbadUpdateFile()

# # Update Simbad database :
# if update_simbad_bool:
#     pass  # TODO coder


